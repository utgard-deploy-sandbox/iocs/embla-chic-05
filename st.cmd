#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(labs-embla_chop-chic-05_VERSION,"plcfactory")
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
iocshLoad("./labs-embla_chop-chic-05.iocsh","IPADDR=172.30.235.99,RECVTIMEOUT=3000")
iocInit()
#EOF